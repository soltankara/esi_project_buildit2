package com.buildit.invoice.application.exception;

public class InvoiceNullException extends Exception {
    public InvoiceNullException() {
        super(String.format("Invoice not exist!"));
    }

}
