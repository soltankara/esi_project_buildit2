package com.buildit.invoice.application.exception;

public class InvoiceNotFoundException extends Exception {
    public InvoiceNotFoundException(Long id) {
        super(String.format("Invoice not found (Invoice id: %d)", id));
    }

}
