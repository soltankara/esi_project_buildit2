package com.buildit.invoice.application.exception;

public class RemittanceAdviceNullException extends Exception {
    public RemittanceAdviceNullException() {
        super(String.format("RemittanceAdvice  not exist!"));
    }
}
