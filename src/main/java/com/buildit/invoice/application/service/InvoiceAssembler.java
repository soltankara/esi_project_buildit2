package com.buildit.invoice.application.service;

import com.buildit.inventory.rest.InventoryRestController;
import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.domain.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDTO> {

    @Autowired
    RemittanceAdviceAssembler remittanceAdviceAssembler;

    public InvoiceAssembler() {
        super(InventoryRestController.class, InvoiceDTO.class);
    }

    @Override
    public InvoiceDTO toResource(Invoice invoice) {

        return InvoiceDTO.of(invoice.getId(),
                invoice.getUrl(),
                invoice.getStatus(),
                remittanceAdviceAssembler.toResource(invoice.getAdvice())
        );
    }
}
