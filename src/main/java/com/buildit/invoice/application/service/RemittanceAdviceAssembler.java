package com.buildit.invoice.application.service;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.dto.RemittanceAdviceDTO;
import com.buildit.invoice.domain.model.Invoice;
import com.buildit.invoice.domain.model.RemittanceAdvice;
import com.buildit.invoice.rest.InvoiceRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class RemittanceAdviceAssembler extends ResourceAssemblerSupport<RemittanceAdvice, RemittanceAdviceDTO> {

    @Autowired
    RemittanceAdviceAssembler remittanceAdviceAssembler;

    public RemittanceAdviceAssembler() {
        super(InvoiceRestController.class, RemittanceAdviceDTO.class);
    }

    @Override
    public RemittanceAdviceDTO toResource(RemittanceAdvice remittanceAdvice) {

        return RemittanceAdviceDTO.of(remittanceAdvice.getId(),
                remittanceAdvice.getAmount(),
                remittanceAdvice.getTo(),
                remittanceAdvice.getFrom());
    }
}
