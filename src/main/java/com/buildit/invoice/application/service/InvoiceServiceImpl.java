package com.buildit.invoice.application.service;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.exception.InvoiceNotFoundException;
import com.buildit.invoice.application.exception.InvoiceNullException;
import com.buildit.invoice.application.parser.InvoiceDTOParser;
import com.buildit.invoice.domain.model.Invoice;
import com.buildit.invoice.domain.model.InvoiceStatus;
import com.buildit.invoice.domain.repository.InvoiceRepository;
import com.buildit.purchase.exception.PurchaseOrderNotFoundException;
import com.buildit.purchase.exception.PurchaseOrderNullException;
import com.buildit.purchase.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    InvoiceDTOParser invoiceDTOParser;
    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    PurchaseOrderService purchaseOrderService;

    public void createInvoice(String invoice) {
        Invoice i = new Invoice();
        i.setUrl(invoice);
        invoiceRepository.save(i);
    }

    @Override
    public List<InvoiceDTO> getInvoices() {
        return invoiceAssembler.toResources(invoiceRepository.findAll());
    }

    public void checkInvoice(String order) throws PurchaseOrderNotFoundException {

        if (purchaseOrderService.getPurchaseOrderByHref(order).size() == 0)
            throw new PurchaseOrderNotFoundException();
    }

    @Override
    public InvoiceDTO approveInvoice(Long id) throws InvoiceNotFoundException, InvoiceNullException {

        Invoice invoice = invoiceRepository.getOne(id);
        if (invoice == null) throw new InvoiceNotFoundException(id);
        invoice.setStatus(InvoiceStatus.APPROVED);
        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    @Override
    public InvoiceDTO rejectInvoice(Long id) throws InvoiceNotFoundException, InvoiceNullException {

        Invoice invoice = invoiceRepository.getOne(id);
        if (invoice == null) throw new InvoiceNotFoundException(id);
        invoice.setStatus(InvoiceStatus.REJECTED);
        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }
}
