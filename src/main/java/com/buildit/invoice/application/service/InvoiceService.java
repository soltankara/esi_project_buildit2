package com.buildit.invoice.application.service;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.exception.InvoiceNotFoundException;
import com.buildit.invoice.application.exception.InvoiceNullException;
import com.buildit.purchase.exception.PurchaseOrderNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InvoiceService {

    void createInvoice(String invoice) ;
    void checkInvoice(String order) throws PurchaseOrderNotFoundException;
    List<InvoiceDTO> getInvoices();
    InvoiceDTO approveInvoice(Long id) throws InvoiceNotFoundException, InvoiceNullException;
    InvoiceDTO rejectInvoice(Long id) throws InvoiceNotFoundException, InvoiceNullException;
}
