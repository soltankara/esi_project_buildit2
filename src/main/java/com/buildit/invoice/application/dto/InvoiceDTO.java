package com.buildit.invoice.application.dto;

import com.buildit.invoice.domain.model.InvoiceStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class InvoiceDTO extends ResourceSupport {

    Long _id;

    String url;

    InvoiceStatus status;

    RemittanceAdviceDTO advice;
}
