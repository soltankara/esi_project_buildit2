package com.buildit.invoice.application.parser;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.dto.RemittanceAdviceDTO;
import com.buildit.invoice.application.exception.InvoiceNullException;
import com.buildit.invoice.application.exception.RemittanceAdviceNullException;
import com.buildit.invoice.domain.model.Invoice;
import com.buildit.invoice.domain.model.RemittanceAdvice;
import org.springframework.stereotype.Component;

@Component
public class RemittanceAdviceDTOParser {

    public RemittanceAdvice parseToRemittanceAdvice(RemittanceAdviceDTO remittanceAdviceDTO) throws RemittanceAdviceNullException {

        if (remittanceAdviceDTO == null) throw new RemittanceAdviceNullException();

        return RemittanceAdvice.of(remittanceAdviceDTO.get_id(),
                remittanceAdviceDTO.getAmount(),
                remittanceAdviceDTO.getTo(),
                remittanceAdviceDTO.getFrom());
    }
}
