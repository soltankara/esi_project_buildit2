package com.buildit.invoice.application.parser;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.exception.InvoiceNullException;
import com.buildit.invoice.application.exception.RemittanceAdviceNullException;
import com.buildit.invoice.domain.model.Invoice;
import com.buildit.invoice.domain.model.RemittanceAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvoiceDTOParser {

    @Autowired
    RemittanceAdviceDTOParser remittanceAdviceDTOParser;

    public Invoice parseToInvoice(InvoiceDTO invoiceDTO) throws InvoiceNullException, RemittanceAdviceNullException {

        if (invoiceDTO == null) throw new InvoiceNullException();

        RemittanceAdvice advice = null;
        if (invoiceDTO.getAdvice() != null)
            advice = remittanceAdviceDTOParser.parseToRemittanceAdvice(invoiceDTO.getAdvice());

        return Invoice.of(invoiceDTO.get_id(),
                invoiceDTO.getUrl(),
                invoiceDTO.getStatus(),
                advice);
    }
}
