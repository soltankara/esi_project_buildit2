package com.buildit.invoice.rest;

import com.buildit.invoice.application.dto.InvoiceDTO;
import com.buildit.invoice.application.exception.InvoiceNotFoundException;
import com.buildit.invoice.application.exception.InvoiceNullException;
import com.buildit.invoice.application.service.InvoiceService;
import com.buildit.invoice.application.service.InvoiceServiceImpl;
import com.buildit.purchase.exception.PurchaseOrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/invoice")
public class InvoiceRestController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<InvoiceDTO>  getInvoices() {
        return invoiceService.getInvoices();
    }

    @GetMapping("/create")
    public ResponseEntity<String>  createInvoice(String invoice) {
        invoiceService.createInvoice(invoice);
        return new ResponseEntity<String>(new String("Invoice Created"), new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/check")
    public ResponseEntity<String> checkInvoice(String order) throws PurchaseOrderNotFoundException {
        invoiceService.checkInvoice(order);
        return new ResponseEntity<String>(new String("PO exist"), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/approve/{id}")
    public ResponseEntity<InvoiceDTO> approveInvoice(@PathVariable Long id) throws InvoiceNotFoundException, InvoiceNullException {
        return new ResponseEntity<InvoiceDTO>(invoiceService.approveInvoice(id), new HttpHeaders(), HttpStatus.ACCEPTED);
    }
    @DeleteMapping("/approve/{id}")
    public ResponseEntity<InvoiceDTO> rejectInvoice(@PathVariable Long id) throws InvoiceNotFoundException, InvoiceNullException {
        return new ResponseEntity<InvoiceDTO>(invoiceService.rejectInvoice(id), new HttpHeaders(), HttpStatus.ACCEPTED);
    }

}
