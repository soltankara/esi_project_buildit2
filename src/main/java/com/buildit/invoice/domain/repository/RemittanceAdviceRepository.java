package com.buildit.invoice.domain.repository;

import com.buildit.invoice.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RemittanceAdviceRepository extends JpaRepository<Invoice, Long> {
}
