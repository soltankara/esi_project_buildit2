package com.buildit.invoice.domain.model;

public enum InvoiceStatus {
    APPROVED,UNAPPROVED,REJECTED,INCORRECT
}
