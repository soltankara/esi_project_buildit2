package com.buildit.invoice.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
public class Invoice {

    @Id @GeneratedValue
    Long id;

    String  url;

    @Enumerated(EnumType.STRING)
    InvoiceStatus status;

    @OneToOne
    RemittanceAdvice advice;
}
