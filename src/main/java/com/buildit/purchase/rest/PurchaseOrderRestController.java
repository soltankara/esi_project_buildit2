package com.buildit.purchase.rest;

import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/orders")
public class PurchaseOrderRestController {

    @Autowired
    PurchaseOrderService purchaseOrderService;

    //C7
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> getPurchaseOrders() {
        return purchaseOrderService.getPurchaseOrders();
    }

}
