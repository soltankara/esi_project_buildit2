package com.buildit.purchase.rest;

import com.buildit.inventory.exception.PlantInventoryEntryNullException;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.exception.*;
import com.buildit.purchase.model.PurchaseOrder;
import com.buildit.purchase.service.PlantHireRequestService;
import com.buildit.purchase.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/hirerequest")
public class PlantHireRequestRestController {

    @Autowired
    PlantHireRequestService plantHireRequestService;
    @Autowired
    PurchaseOrderService purchaseOrderService;

    @PostMapping("/")
    public ResponseEntity<PlantHireRequestDTO> createPlantHireRequest(@RequestBody PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException {

        PlantHireRequestDTO responseDTO = plantHireRequestService.createPlantHireRequest(requestDTO);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.CREATED);
    }

    // CC5
    @PostMapping("/update")
    public ResponseEntity<PlantHireRequestDTO> updatePlantHireRequest(@RequestBody PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PlantHireRequestNotFoundException, PlantHireRequestApproved {

        PlantHireRequestDTO responseDTO = plantHireRequestService.updatePlantHireRequest(requestDTO);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.CREATED);
    }

    // C8
    @PostMapping("/extend")
    public ResponseEntity<String> extendPlantHireRequest(@RequestBody PlantHireRequestDTO requestDTO) throws PlantHireRequestNullException, PlantHireRequestExtendException, PlantHireRequestNotFoundException {

        plantHireRequestService.extendPlantHireRequest(requestDTO);
        return new ResponseEntity<>(new String("Request sent to RENTIT"), new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/pendingrequests")
    @ResponseStatus(HttpStatus.OK)
    public List<PlantHireRequestDTO> getPendingPlantHireRequests() {
        return plantHireRequestService.getPendingPlantHireRequests();
    }

    // this method is CC4 req.
    @GetMapping("/{id}")
    public ResponseEntity<PlantHireRequestDTO> findPlantHireRequest(@PathVariable Long id) throws PlantHireRequestNotFoundException {
        PlantHireRequestDTO responseDTO = plantHireRequestService.findPlantHireRequest(id);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/accept")
    public ResponseEntity<PlantHireRequestDTO> acceptPlantHireRequest(@RequestBody PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PurchaseOrderCreatedException {

        PlantHireRequestDTO responseDTO = plantHireRequestService.acceptPlantHireRequest(requestDTO);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.ACCEPTED);
    }

    @PostMapping("/reject")
    public ResponseEntity<PlantHireRequestDTO> rejectPlantHireRequest(@RequestBody PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException {

        PlantHireRequestDTO responseDTO = plantHireRequestService.rejectPlantHireRequest(requestDTO);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<PlantHireRequestDTO> cancelPlantHireRequest(@PathVariable Long id) throws PlantHireRequestNullException, PlantHireRequestNotFoundException, PlantHireRequestCancellationException, PurchaseOrderCreatedException {

        PlantHireRequestDTO responseDTO = plantHireRequestService.cancelPlantHireRequest(id);
        return new ResponseEntity<>(responseDTO, new HttpHeaders(), HttpStatus.ACCEPTED);
    }



}
