package com.buildit.purchase.service;

import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Override
    public List<PurchaseOrderDTO> getPurchaseOrders() {
        return purchaseOrderAssembler.toResources(purchaseOrderRepository.findAll());
    }
    @Override
    public List<PurchaseOrderDTO> getPurchaseOrderByHref(String href){
        return purchaseOrderAssembler.toResources(
                purchaseOrderRepository.findPurchaseOrdersByHrefContaining(href)
        );
    }
}
