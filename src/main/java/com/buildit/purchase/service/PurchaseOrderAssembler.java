package com.buildit.purchase.service;

import com.buildit.purchase.rest.PlantHireRequestRestController;
import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.model.PurchaseOrder;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {

    public PurchaseOrderAssembler() {
        super(PlantHireRequestRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toResource(PurchaseOrder po) {
        PurchaseOrderDTO dto = new PurchaseOrderDTO();
        dto.setHref(po.getHref());
        dto.set_Id(po.getId());
        if (dto.getHref() != null) {
            try {
                dto.setLinks( new ArrayList<>());
                dto.getLinks().add(new Link(dto.getHref()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dto;
    }
}
