package com.buildit.purchase.service;

import com.buildit.purchase.dto.PurchaseOrderDTO;

import java.util.List;

public interface PurchaseOrderService {
    List<PurchaseOrderDTO> getPurchaseOrders();

    List<PurchaseOrderDTO> getPurchaseOrderByHref(String href);
}
