package com.buildit.purchase.service;

import com.buildit.common.dto.BusinessPeriodDTO;
import com.buildit.inventory.service.PlantInventoryEntryAssembler;
import com.buildit.purchase.rest.PlantHireRequestRestController;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.exception.PlantHireRequestNotFoundException;
import com.buildit.purchase.model.PlantHireRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {

    public PlantHireRequestAssembler() {
        super(PlantHireRequestRestController.class, PlantHireRequestDTO.class);
    }

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Override
    public PlantHireRequestDTO toResource(PlantHireRequest request) {
        PlantHireRequestDTO dto = new PlantHireRequestDTO();

        dto.set_id(request.getId());
        dto.setPlant(plantInventoryEntryAssembler.toResource(request.getPlant()));
        dto.setSupplierName(request.getSupplierName());
        dto.setSiteName(request.getSiteName());
        dto.setCost(request.getCost());
        if (request.getPurchaseOrder() != null)
            dto.setPurchaseOrder(purchaseOrderAssembler.toResource(request.getPurchaseOrder()));
        dto.setPeriod(BusinessPeriodDTO.of(request.getPeriod().getStartDate(), request.getPeriod().getEndDate()));
        try {
            dto.add(linkTo(methodOn(PlantHireRequestRestController.class)
                    .findPlantHireRequest(dto.get_id())).withRel("self"));
        } catch (PlantHireRequestNotFoundException e) {
            e.printStackTrace();
        }
        dto.setComment(request.getComment());
        dto.setStatus(request.getStatus());
        return dto;
    }
}
