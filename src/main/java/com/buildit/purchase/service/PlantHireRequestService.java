package com.buildit.purchase.service;

import com.buildit.inventory.exception.PlantInventoryEntryNullException;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.exception.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PlantHireRequestService {

    PlantHireRequestDTO createPlantHireRequest(PlantHireRequestDTO partialDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException;

    PlantHireRequestDTO updatePlantHireRequest(PlantHireRequestDTO partialDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PlantHireRequestNotFoundException, PlantHireRequestApproved;

    void extendPlantHireRequest(PlantHireRequestDTO partialDTO) throws PlantHireRequestNullException, PlantHireRequestNotFoundException, PlantHireRequestExtendException;

    PlantHireRequestDTO acceptPlantHireRequest(PlantHireRequestDTO partialDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PurchaseOrderCreatedException;

    PlantHireRequestDTO rejectPlantHireRequest(PlantHireRequestDTO partialDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException;

    PlantHireRequestDTO cancelPlantHireRequest(Long id) throws PlantHireRequestNullException, PlantHireRequestNotFoundException, PlantHireRequestCancellationException, PurchaseOrderCreatedException;

    PlantHireRequestDTO findPlantHireRequest(Long id) throws PlantHireRequestNotFoundException;

    PurchaseOrderDTO createPO(PlantHireRequestDTO dto) throws PurchaseOrderCreatedException;

    List<PlantHireRequestDTO> getPendingPlantHireRequests();
}
