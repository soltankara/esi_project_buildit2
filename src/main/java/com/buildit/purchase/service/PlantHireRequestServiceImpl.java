package com.buildit.purchase.service;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.inventory.exception.PlantInventoryEntryNullException;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.exception.*;
import com.buildit.purchase.model.PlantHireRequest;
import com.buildit.purchase.model.PlantHireRequestStatus;
import com.buildit.purchase.model.PurchaseOrder;
import com.buildit.purchase.parser.PlantHireRequestDTOParser;
import com.buildit.purchase.repository.PlantHireRequestRepository;
import com.buildit.purchase.repository.PurchaseOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.xml.ws.Response;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlantHireRequestServiceImpl implements PlantHireRequestService {

    @Autowired
    PlantHireRequestDTOParser plantHireRequestDTOParser;
    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;
    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    public PlantHireRequestDTO createPlantHireRequest(PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException {
        requestDTO.setStatus(PlantHireRequestStatus.PENDING);
        requestDTO.setPurchaseOrder(null);
        PlantHireRequest responseHireRequestObj = plantHireRequestDTOParser.parseToPlantHireRequest(requestDTO);
        responseHireRequestObj = plantHireRequestRepository.save(responseHireRequestObj);
        return plantHireRequestAssembler.toResource(responseHireRequestObj);
    }

    public PlantHireRequestDTO updatePlantHireRequest(PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PlantHireRequestNotFoundException, PlantHireRequestApproved {

        PlantHireRequest responseHireRequestObj = plantHireRequestRepository.getOne(requestDTO.get_id());
        if (responseHireRequestObj == null)
            throw new PlantHireRequestNotFoundException(requestDTO.get_id());
        else if (responseHireRequestObj.getStatus() != PlantHireRequestStatus.PENDING)
            throw new PlantHireRequestApproved(requestDTO.get_id());

        responseHireRequestObj = plantHireRequestDTOParser.parseToPlantHireRequest(requestDTO);
        responseHireRequestObj = plantHireRequestRepository.save(responseHireRequestObj);
        return plantHireRequestAssembler.toResource(responseHireRequestObj);
    }

    public void extendPlantHireRequest(PlantHireRequestDTO requestDTO) throws PlantHireRequestNotFoundException, PlantHireRequestExtendException {

        PlantHireRequest responseHireRequestObj = plantHireRequestRepository.getOne(requestDTO.get_id());
        if (responseHireRequestObj == null)
            throw new PlantHireRequestNotFoundException(requestDTO.get_id());
        extendPO(requestDTO);
    }

    public PlantHireRequestDTO acceptPlantHireRequest(PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException, PurchaseOrderCreatedException {
        PlantHireRequest hireRequest = plantHireRequestDTOParser.parseToPlantHireRequest(requestDTO);

        PlantHireRequestDTO hireRequestDTO = plantHireRequestAssembler.toResource(hireRequest);
        hireRequestDTO.setPurchaseOrder(createPO(hireRequestDTO));

        hireRequestDTO.setStatus(PlantHireRequestStatus.ACCEPTED);
        hireRequest = plantHireRequestDTOParser.parseToPlantHireRequest(hireRequestDTO);

        hireRequest.setPurchaseOrder(purchaseOrderRepository.save(hireRequest.getPurchaseOrder()));
        hireRequest = plantHireRequestRepository.save(hireRequest);

        return plantHireRequestAssembler.toResource(hireRequest);
    }

    public PlantHireRequestDTO rejectPlantHireRequest(PlantHireRequestDTO requestDTO) throws PlantInventoryEntryNullException, PlantHireRequestNullException, PurchaseOrderNullException {
        PlantHireRequest hireRequest = plantHireRequestDTOParser.parseToPlantHireRequest(requestDTO);
        hireRequest.setStatus(PlantHireRequestStatus.REJECTED);
        hireRequest = plantHireRequestRepository.save(hireRequest);
        return plantHireRequestAssembler.toResource(hireRequest);
    }

    public PlantHireRequestDTO cancelPlantHireRequest(Long id) throws PlantHireRequestNotFoundException, PlantHireRequestCancellationException, PurchaseOrderCreatedException {

        PlantHireRequest hireRequest = plantHireRequestRepository.getOne(id);
        if (hireRequest == null)
            throw new PlantHireRequestNotFoundException(id);
        else if (hireRequest.getPeriod().getEndDate().isBefore(LocalDate.now())){

            ResponseEntity<PurchaseOrderDTO> responseEntity =
                    restTemplate.getForEntity("http://localhost:8080/api/sales/orders/{oid}/cancel",
                            PurchaseOrderDTO.class, hireRequest.getId());
            if (responseEntity.getStatusCode() != HttpStatus.CREATED) {
                throw new PlantHireRequestCancellationException(hireRequest.getId());
            }
        }

        hireRequest.setStatus(PlantHireRequestStatus.CANCELLED);
        hireRequest = plantHireRequestRepository.save(hireRequest);
        return plantHireRequestAssembler.toResource(hireRequest);
    }


    public PlantHireRequestDTO findPlantHireRequest(Long id) throws PlantHireRequestNotFoundException {
        PlantHireRequest hireRequest = plantHireRequestRepository.getOne(id);

        if (hireRequest == null) throw new PlantHireRequestNotFoundException(id);

        return plantHireRequestAssembler.toResource(hireRequest);
    }

    RestTemplate restTemplate = new RestTemplate();

    public PurchaseOrderDTO createPO(PlantHireRequestDTO requestDTO) throws PurchaseOrderCreatedException {

//        ResponseEntity<PurchaseOrderDTO> responseEntity =
//                restTemplate.getForEntity("http://localhost:8080/api/sales/orders/create?plant={href}&startDate={startDate}&endDate={endDate}", PurchaseOrderDTO.class
//                        , requestDTO.getPlant().getHref(), requestDTO.getPeriod().getStartDate(), requestDTO.getPeriod().getEndDate());
        JSONObject obj = new JSONObject();
        obj.put("invoiceHref","asd");
        obj.put("statusHref","asd");
        obj.put("plant",requestDTO.getPlant());
        obj.put("rentalPeriod",requestDTO.getPeriod());


        HttpEntity<PlantHireRequestDTO> request = new HttpEntity<>(requestDTO);
        String result = restTemplate.postForObject("https://private-82dcb-rentit43.apiary-mock.com/api/sales/orders", request, String.class);
        System.out.println(result);

        JSONObject ob = new JSONObject(result);


        PurchaseOrderDTO responseHireRequestDTO = new PurchaseOrderDTO();
        responseHireRequestDTO.setHref(ob.getString("statusHref"));
        return responseHireRequestDTO;
    }

    public void extendPO(PlantHireRequestDTO requestDTO) throws PlantHireRequestExtendException {

        ResponseEntity<PurchaseOrderDTO> responseEntity =
                restTemplate.getForEntity("http://localhost:8080/api/sales/orders/extend?order={href}&endDate={endDate}", PurchaseOrderDTO.class
                        , requestDTO.getPurchaseOrder().getHref(), requestDTO.getPeriod().getEndDate());
        if (responseEntity.getStatusCode() != HttpStatus.CREATED)
            throw new PlantHireRequestExtendException(requestDTO.get_id());
    }

    public List<PlantHireRequestDTO> getPendingPlantHireRequests() {
        List<PlantHireRequest> pendingPlantHireRequests = plantHireRequestRepository.findPendingPlantHireRequests();
        return plantHireRequestAssembler.toResources(pendingPlantHireRequests);
    }
}
