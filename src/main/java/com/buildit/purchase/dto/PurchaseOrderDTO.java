package com.buildit.purchase.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
public class PurchaseOrderDTO extends ResourceSupport {

    List<Link> links = new ArrayList<>();
    String href;
    Long _Id;

    public void bindHref() {
        for (Link link : links) {
            if (link.getRel().equals("self")) {
                this.href = link.getHref();
            }
        }
        links = null;
    }
}
