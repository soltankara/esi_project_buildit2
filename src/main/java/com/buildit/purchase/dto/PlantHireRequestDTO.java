package com.buildit.purchase.dto;

import com.buildit.common.dto.BusinessPeriodDTO;
import com.buildit.common.model.BusinessPeriod;
import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.purchase.model.PlantHireRequest;
import com.buildit.purchase.model.PlantHireRequestStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
public class PlantHireRequestDTO extends ResourceSupport {

    Long _id;
    String siteName;
    String supplierName;
    PlantInventoryEntryDTO plant;
    BusinessPeriodDTO period;
    @Column(precision = 8, scale = 2)
    BigDecimal cost;
    String comment;

    PlantHireRequestStatus status;

    PurchaseOrderDTO purchaseOrder;


}

