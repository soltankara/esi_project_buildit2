package com.buildit.purchase.exception;

public class PlantHireRequestNotFoundException extends Exception {
    public PlantHireRequestNotFoundException(Long id) {
        super(String.format("Plant hire request not found! (Plant Hire Request id: %d)", id));
    }

}
