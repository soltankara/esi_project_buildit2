package com.buildit.purchase.exception;

public class PlantHireRequestApproved extends Throwable {
    public PlantHireRequestApproved(Long id) {
        super(String.format("Plant hire request already approved! (Plant Hire Request id: %d)", id));
    }
}
