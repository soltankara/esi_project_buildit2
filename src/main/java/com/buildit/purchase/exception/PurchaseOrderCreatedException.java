package com.buildit.purchase.exception;

public class PurchaseOrderCreatedException extends Exception {
    public PurchaseOrderCreatedException() {
        super(String.format("PurchaseOrder can not be created!"));
    }
}
