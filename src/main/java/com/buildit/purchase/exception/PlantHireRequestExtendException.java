package com.buildit.purchase.exception;

public class PlantHireRequestExtendException extends Exception{
    public PlantHireRequestExtendException(Long id) {
        super(String.format("Plant hire request can not be extended! (Plant Hire Request id: %d)", id));
    }
}
