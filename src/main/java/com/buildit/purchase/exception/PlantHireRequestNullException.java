package com.buildit.purchase.exception;

public class PlantHireRequestNullException extends Exception {
    public PlantHireRequestNullException() {
        super(String.format("PlantHireRequest can not be null!"));
    }
}
