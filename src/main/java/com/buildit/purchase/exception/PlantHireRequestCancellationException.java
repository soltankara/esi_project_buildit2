package com.buildit.purchase.exception;

public class PlantHireRequestCancellationException extends Exception {
    public PlantHireRequestCancellationException(Long id) {
        super(String.format("Plant hire request can not be cancelled due to end data! (Plant Hire Request id: %d)", id));
    }
}
