package com.buildit.purchase.parser;

import com.buildit.common.model.BusinessPeriod;
import com.buildit.inventory.exception.PlantInventoryEntryNullException;
import com.buildit.inventory.parser.PlantInventoryEntryDTOParser;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.exception.PlantHireRequestNullException;
import com.buildit.purchase.exception.PurchaseOrderNullException;
import com.buildit.purchase.model.PlantHireRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlantHireRequestDTOParser {

    @Autowired
    PlantInventoryEntryDTOParser plantInventoryEntryDTOParser;

    @Autowired
    PurchaseOrderDTOParser purchaseOrderDTOParser;


    public PlantHireRequest parseToPlantHireRequest(PlantHireRequestDTO hireRequestDTO)
            throws PlantHireRequestNullException, PlantInventoryEntryNullException, PurchaseOrderNullException {

        if (hireRequestDTO.equals(null)) throw new PlantHireRequestNullException();

        return PlantHireRequest.of(
                hireRequestDTO.get_id(),
                hireRequestDTO.getSiteName(),
                hireRequestDTO.getSupplierName(),
                plantInventoryEntryDTOParser.parseToPlantInventoryEntry(hireRequestDTO.getPlant()),
                BusinessPeriod.of(hireRequestDTO.getPeriod().getStartDate(),
                        hireRequestDTO.getPeriod().getEndDate()),
                hireRequestDTO.getCost(),
                hireRequestDTO.getComment(),
                hireRequestDTO.getStatus(),
                purchaseOrderDTOParser.parseToPurchaseOrder(hireRequestDTO.getPurchaseOrder()));
    }
}
