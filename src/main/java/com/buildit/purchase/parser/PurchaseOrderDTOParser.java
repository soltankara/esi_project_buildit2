package com.buildit.purchase.parser;

import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.exception.PurchaseOrderNullException;
import com.buildit.purchase.model.PurchaseOrder;
import org.springframework.stereotype.Component;

@Component
public class PurchaseOrderDTOParser {

    public PurchaseOrder parseToPurchaseOrder(PurchaseOrderDTO poDTO) throws PurchaseOrderNullException {

        if (poDTO == null) return null;

        return PurchaseOrder.of(poDTO.get_Id(),poDTO.getHref());
    }
}
