package com.buildit.purchase.repository;

import com.buildit.purchase.model.PlantHireRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantHireRequestRepository extends JpaRepository<PlantHireRequest, Long>
{
    @Query("SELECT requests from PlantHireRequest requests where requests.status = 'PENDING'")
    List<PlantHireRequest> findPendingPlantHireRequests();
}
