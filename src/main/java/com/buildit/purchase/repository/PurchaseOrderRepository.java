package com.buildit.purchase.repository;

import com.buildit.purchase.dto.PurchaseOrderDTO;
import com.buildit.purchase.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, PurchaseOrderDTO> {

    List<PurchaseOrder> findPurchaseOrdersByHrefContaining(String href);
}
