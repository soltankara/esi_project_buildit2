package com.buildit.purchase.model;

import com.buildit.common.model.BusinessPeriod;
import com.buildit.inventory.model.PlantInventoryEntry;
import com.buildit.purchase.dto.PurchaseOrderDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class PlantHireRequest {

    @Id
    @GeneratedValue
    Long Id;

    String siteName;

    String supplierName;

    @OneToOne(cascade = CascadeType.ALL)
    PlantInventoryEntry plant;

    @Embedded
    BusinessPeriod period;

    @Column(precision = 8, scale = 2)
    BigDecimal cost;
    String comment;

    @Enumerated(EnumType.STRING)
    PlantHireRequestStatus status;
    @OneToOne(cascade = CascadeType.ALL)
    PurchaseOrder purchaseOrder;
}
