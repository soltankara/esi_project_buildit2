package com.buildit.purchase.model;

public enum PlantHireRequestStatus {
    ACCEPTED,REJECTED,PENDING,CANCELLED
}
