package com.buildit.purchase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class PurchaseOrder {

    @Id
    @GeneratedValue
    Long Id;

    String href;
}
