package com.buildit.inventory.dto;

import lombok.*;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
public class PlantInventoryEntryDTO extends ResourceSupport {

    List<org.springframework.hateoas.Link> links = new ArrayList<>();
    Long identifier;
    String name;
    BigDecimal price;
    String href;
    String description;

    public void bindHref() {
        for (Link link : links) {
            if (link.getRel().equals("self")) {
                this.href = link.getHref();
            }
        }
//        links = null;
    }
}
