package com.buildit.inventory.service;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.inventory.model.PlantInventoryEntry;
import com.buildit.inventory.rest.InventoryRestController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {

    public PlantInventoryEntryAssembler() {
        super(InventoryRestController.class, PlantInventoryEntryDTO.class);
    }

    @Override
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO();
        dto.setName(plant.getName());
        dto.setHref(plant.getHref());
        dto.setDescription(plant.getDescription());
        dto.setIdentifier(plant.getIdentifier());
        if (dto.getHref() != null) {
            try {
                dto.setLinks( new ArrayList<>());
                dto.getLinks().add(new Link(dto.getHref()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dto;
    }
}
