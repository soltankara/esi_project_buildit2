package com.buildit.inventory.service;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;


import java.time.LocalDate;
import java.util.List;


public interface PlantInventoryEntryService {

    List<PlantInventoryEntryDTO> getPlantCatalog(String name, LocalDate startDate, LocalDate endDate);

//    Boolean checkAvailabilityPlant(Long id, LocalDate startDate, LocalDate endDate);
}