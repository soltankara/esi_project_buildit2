package com.buildit.inventory.service;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public @Service
class PlantInventoryEntryServiceImpl implements PlantInventoryEntryService {
    RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<PlantInventoryEntryDTO> getPlantCatalog(String name, LocalDate startDate, LocalDate endDate)
    {
        PlantInventoryEntryDTO[] plantInventoryEntryList =
//                restTemplate.getForObject("http://localhost:8080/api/sales/plants?name={name}&startDate={startDate}&endDate={endDate}",
                restTemplate.getForObject("https://private-82dcb-rentit43.apiary-mock.com/api/inventory/entries?endDate={endDate}&name={name}&startDate={startDate}",
                PlantInventoryEntryDTO[].class, endDate,name, startDate);

        for (PlantInventoryEntryDTO entryDTO : plantInventoryEntryList) {
            entryDTO.bindHref();
            System.out.println(entryDTO);
        }

        return Arrays.asList(plantInventoryEntryList);
    }



    /*
    *
    * https://private-82dcb-rentit43.apiary-mock.com/api/inventory/entries?endDate=2018-07-19&id=1&name=excavator&startDate=2018-07-12
    *
    * */

//    @Override
//    public Boolean checkAvailabilityPlant(Long id, LocalDate startDate, LocalDate endDate) {
//
//        return restTemplate.getForObject("http://localhost:8080/api/inventory/checkAvailabilityPlant?name={name}&startDate={startDate}&endDate={endDate}",
//                Boolean.class, id, startDate, endDate);
//    }

}