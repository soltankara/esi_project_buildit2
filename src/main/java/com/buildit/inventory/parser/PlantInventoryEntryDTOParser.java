package com.buildit.inventory.parser;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.inventory.exception.PlantInventoryEntryNullException;
import com.buildit.inventory.model.PlantInventoryEntry;
import org.springframework.stereotype.Component;

@Component
public class PlantInventoryEntryDTOParser {

    public PlantInventoryEntry parseToPlantInventoryEntry(PlantInventoryEntryDTO entryDTO) throws PlantInventoryEntryNullException {

        if (entryDTO.equals(null)) throw new PlantInventoryEntryNullException();

        return PlantInventoryEntry.of(
                entryDTO.getIdentifier(),
                entryDTO.getHref(),
                entryDTO.getName(),
                entryDTO.getDescription());
    }
}
