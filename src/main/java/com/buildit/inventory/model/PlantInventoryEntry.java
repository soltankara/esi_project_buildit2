package com.buildit.inventory.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
public class PlantInventoryEntry {

    @Id
    @GeneratedValue
    Long identifier;
    String href;
    String name;
    String description;

    public PlantInventoryEntry(String id, String href, String name) {
        this.identifier = Long.parseLong(id);
        this.href = href;
        this.name = name;
    }
}
