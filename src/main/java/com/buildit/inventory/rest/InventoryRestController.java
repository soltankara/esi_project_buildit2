package com.buildit.inventory.rest;

import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.inventory.service.PlantInventoryEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/api/inventory")
public class InventoryRestController {

    @Autowired
    private ApplicationContext context;

    @GetMapping("/plantCatalog")
    public List<PlantInventoryEntryDTO> getPlantCatalog(@RequestParam(name = "name", required = false) String name,
                                                        @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                        @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {

        PlantInventoryEntryService plantInventoryEntryService =
                context.getBean(PlantInventoryEntryService.class);

        return plantInventoryEntryService.getPlantCatalog(name, startDate, endDate);
    }
}
