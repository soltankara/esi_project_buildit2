package com.buildit.inventory.exception;

public class PlantInventoryEntryNullException extends Exception {
    public PlantInventoryEntryNullException() {
        super(String.format("PlantInventoryEntry can not be null!"));
    }
}
