Feature: Creation of Purchase Order
  As a Rentit's customer
  So that I start with the construction project
  I want hire all the required machinery

  Background: Plant catalog
    Given BuildIT employee is on the site homepage

  Scenario: Acceptance of Purchase Order
    When BuildIT employee navigated to homepage
    Then 3 plants are shown
    Then BuildIT employee selects a "Mini excavator"
    Then BuildIT employee fills order details and creates plant hire request from "2018-05-20" to "2018-05-22"
    Then BuildIT employee accepts the order
    Then navigate to RentIT site homepage
    Then RentIT employee accepts the PO
    Then navigate to BuildIT homepage
    Then BuildIT employee is notified
    Then BuildIT employee checks status of Plant Hire Request

  Scenario: Rejection of Purchase Order
    When BuildIT employee navigated to homepage
    Then 3 plants are shown
    Then BuildIT employee selects a "Mini excavator"
    Then BuildIT employee fills order details and creates plant hire request from "2018-05-20" to "2018-05-22"
    Then BuildIT employee accepts the order
    Then navigate to RentIT site homepage
    Then RentIT employee rejects the PO

  Scenario: Rejection of Plant Hire Request
    When BuildIT employee navigated to homepage
    Then 3 plants are shown
    Then BuildIT employee selects a "Mini excavator"
    Then BuildIT employee fills order details and creates plant hire request from "2018-05-20" to "2018-05-22"
    Then BuildIT employee rejects the order