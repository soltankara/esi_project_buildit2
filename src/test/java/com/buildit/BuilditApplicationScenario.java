package com.buildit;

import com.buildit.inventory.model.PlantInventoryEntry;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import gherkin.lexer.Th;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class BuilditApplicationScenario {

    WebDriver customer;
    RestTemplate restTemplate = new RestTemplate();

    static {
        System.setProperty("webdriver.chrome.driver", "C:/Users/soltankara/Desktop/chromedriver.exe");
    }

    @Before
    public void setup() {
        customer = new ChromeDriver();
    }

    @After
    public void tearoff() {
        customer.close();
    }

    @Given("^BuildIT employee is on the site homepage$")
    public void buildit_employee_is_on_the_site_homepage() throws Exception {
        System.out.println("given");
    }

    @When("^BuildIT employee navigated to homepage$")
    public void buildit_employee_navigated_to_homepage() throws Exception {
        customer.get("http://localhost:8081/#/");
    }

    @Then("^(\\d+) plants are shown$")
    public void plants_are_shown(int numberOfPlants) throws Exception {
        Thread.sleep(3000);
        List<?> rows = customer.findElements(By.className("table-row"));
        assertThat(rows).hasSize(numberOfPlants);
    }

    @Then("^BuildIT employee selects a \"([^\"]*)\"$")
    public void buildit_employee_selects_a(String plantName) throws Exception {
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", plantName)));
        WebElement selectPlantButton = row.findElement(By.xpath("//a[contains(text(), 'Hire Plant')]"));
        selectPlantButton.click();
    }

    @Then("^BuildIT employee fills order details and creates plant hire request from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void buildit_employee_fills_order_details_and_creates_plant_hire_request_from_to(String startDate, String endDate) throws Exception {
        Thread.sleep(1000);
        customer.switchTo().activeElement();
        customer.findElement(By.id("start-date")).sendKeys(startDate);
        customer.findElement(By.id("end-date")).sendKeys(endDate);
        customer.findElement(By.id("construction-site-name")).sendKeys("BuildIT");
        customer.findElement(By.id("supplier-name")).sendKeys("RentIT");
        customer.findElement(By.id("cost")).sendKeys("1500.00");
        customer.findElement(By.id("accept-plant-modal")).click();
    }

    @Then("^BuildIT employee accepts the order$")
    public void buildit_employee_accepts_the_order() throws Exception {
        Thread.sleep(2000);
        customer.get("http://localhost:8081/#/pendingrequests");
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement selectPlantButton = row.findElement(By.xpath("//a[contains(text(), 'See Details')]"));
        selectPlantButton.click();
        customer.findElement(By.id("btn-accept")).click();
    }

    @Then("^BuildIT employee rejects the order$")
    public void buildit_employee_rejects_the_order() throws Exception {
        Thread.sleep(2000);
        customer.get("http://localhost:8081/#/pendingrequests");
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement selectPlantButton = row.findElement(By.xpath("//a[contains(text(), 'See Details')]"));
        selectPlantButton.click();
        customer.findElement(By.id("btn-reject")).click();
    }

    @Then("^navigate to RentIT site homepage$")
    public void navigate_to_RentIT_site_homepage() throws Exception {
        Thread.sleep(3000);
        customer.get("http://localhost:8082/#");
        customer.findElement(By.id("select-plant")).click();
    }

    @Then("^RentIT employee accepts the PO$")
    public void rentit_employee_accepts_the_PO() throws Exception {
        Thread.sleep(3000);
        customer.findElement(By.id("btn-accept")).click();
    }

    @Then("^RentIT employee rejects the PO$")
    public void rentit_employee_rejects_the_PO() throws Exception {
        Thread.sleep(3000);
        customer.findElement(By.id("btn-reject")).click();
    }

    @Then("^navigate to BuildIT homepage$")
    public void navigate_to_BuildIT_homepage() throws Exception {
        Thread.sleep(2000);
        customer.get("http://localhost:8081/#");
    }

    @Then("^BuildIT employee is notified$")
    public void buildit_employee_is_notified() throws Exception {
    }

    @Then("^BuildIT employee checks status of Plant Hire Request$")
    public void buildit_employee_checks_status_of_Plant_Hire_Request() throws Exception {
    }

}