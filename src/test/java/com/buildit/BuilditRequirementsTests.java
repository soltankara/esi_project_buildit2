package com.buildit;

import com.buildit.common.dto.BusinessPeriodDTO;
import com.buildit.common.model.BusinessPeriod;
import com.buildit.inventory.dto.PlantInventoryEntryDTO;
import com.buildit.inventory.model.PlantInventoryEntry;
import com.buildit.purchase.dto.PlantHireRequestDTO;
import com.buildit.purchase.model.PlantHireRequest;
import com.buildit.purchase.model.PlantHireRequestStatus;
import com.buildit.purchase.repository.PlantHireRequestRepository;
import com.buildit.purchase.service.PlantHireRequestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.jni.Local;
import org.h2.table.Plan;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BuilditApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BuilditRequirementsTests {
    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    PlantHireRequestService plantHireRequestService;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    //Postmanda çalışıyor ama burada 404 atıyor
    public void cc1() throws Exception {
        PlantInventoryEntryDTO plant = new PlantInventoryEntryDTO();
        plant.setName("Truck");
        plant.setHref("http://rentit.com/plants/skd2231asa");
        PlantHireRequestDTO phr = new PlantHireRequestDTO();
        phr.setPlant(plant);
        phr.setPeriod(BusinessPeriodDTO.of(LocalDate.now().plusDays(10), LocalDate.now().plusDays(12)));

        System.out.println(mapper.writeValueAsString(phr));

        mockMvc.perform(post("/hirerequest")
                .content(mapper.writeValueAsString(phr))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void cc2() {

    }

    @Test
    public void cc3() {

    }

    @Test
    public void cc4() {

    }

    @Test
    public void cc5() {

    }

    @Test
    public void cc6() {

    }

    @Test
    public void cc7() {

    }

    @Test
    public void cc8() {

    }

    @Test
    public void cc9() {

    }

    @Test
    public void cc10() {

    }

    @Test
    public void cc11() {

    }

    @Test
    public void cc12() {

    }
}
